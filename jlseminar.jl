#= Julia Language Seminar =#
# Ken Sible | September 10, 2018
# Repository: https://gitlab.com/ksible21
# Documentation: https://docs.julialang.org

#= Introduction
    This seminar introduces the Julia language for those with some prior programming
    experience, and explores the many features that make this language powerful.
    Julia was designed for high performance numerical computing and data science
    while having the concise syntax of scripting languages, such as Python/MATLAB,
    and the static-typed performance of compiled languages such as C/C++/Fortran.
    The primary goal for this language was solving the ‘two-language problem’,
    wherein scientists would program using high-level languages while rewriting
    the performance-critical aspects using lower-level compiled languages.
    The Julia language solves this problem with type declarations, parametric
    polymorphism, and multiple dispatch; where the language dispatches on
    type-stable function calls for optimized performance. Julia can compile these
    functions with type stability, provided that internal function calls are
    type-stable, while performing optimizations that makes the compiled functions
    equivalent with C/C++/Fortran. The language developers confidently stated that 
    Julia could “achieve machine performance without sacrificing human convenience.”
=#

#= Seminar Topics
    1. Variables and Math Operations
    2. Complex and Rational Numbers
    3. Strings (Character Sequences)
    4. Functions (Map: Tuple -> Value)
    5. Vectorized Functions (Broadcast)
    6. Control Flow (If-Else, Loops)
    7. Variable Scope (Local, Global)
    8. Types (Julia Type System)
    9. Methods (Multiple Dispatch)
    10. Conversion and Promotion
    11. Multi-Dimensional Arrays
    12. Interfaces (Iteration)
    13. Modules (Variable Workspaces)
    14. Metaprogramming (Expressions)
=#

#= Julia Enviroment
    $ julia script.jl arg1 arg2 ...
    => ARG = (arg1, arg2, ...) (global constant)
    include("file.jl") evaluates expressions
    '?' enters help mode and ';' enters shell mode
    ']' accesses the package manager (Julia v1.0)
=#

#= Unicode Variable Names =#
δ = 0.01 # \delta (tab) [LaTeX]

#= Numeric Literal Coefficents =#
x = 3; println(2x^2 - 3x + 1) # Implied Multiplication

#= Mathematical Operations =#
x = x + 1; x += 1 # Updating Operators
isfinite(x), isinf(x), isnan(x) # Test Special Values
2 < x < 5; 1 < 2 <= 2 < 3 == 3 # Chained Comparisons
Int(1.0); convert(Int, 1.0) # Numerical Conversions
div(5, 3); println(5 ÷ 3) # Integer Division
round(x); floor(x); ceil(x); trunc(x) # Rounding
abs(x); sign(x); sqrt(x); exp(x); log(x); log(2, x)
sin(x); sinh(x); asin(x); asinh(x); sinc(x); sind(x)

#= Complex and Rational Numbers =#
z = 1 + 2im # Complex Number [im ≡ √(-1)]
real(z); imag(z) # Real and Imaginary Parts
abs(z); angle(z) # Magnitude and Phase Angle
conj(z) # Complex Conjugate
sqrt(-1); sqrt(-1 + 0im) # Avoid Domain Error
a = 1; b = 2; complex(a, b) # Construct Complex
k = 2//3 # Rational Number (Ratio of Integers)
numerator(k); denominator(k) # Extract Integers
float(3//4) # Convert Rational -> Float
println(5//0) # Infinite Rational
println(0//0) # NaN Rational -> Error
println(1//3 - 0.33) # Type Promotion

#= Strings (Character Sequences) =#
forall = '\u2200' # Unicode Character (∀)
str = "Hello There!"; length(str) # String Literal
str[1]; str[end]; str[1:5] # Extract Character/Substring
greeting = "Hello"; name = "Ken"
println(greeting * ", " * name) # String Concatenation
println("$greeting, $(name)!") # String Interpolation
findfirst(); findnext(); occursin() # String Functions

#= Functions (Map: Tuple -> Value) =#
function f(x, y)
    x + y
end
f(x, y) = x + y # Compact "Assignment Form"
Σ(x, y) = x + y # Unicode Function Name
g = f; g(2, 3) # Function Object
1 + 2; +(1, 2) # Operators <=> Functions
(x -> x^2 + 2x - 1)(5) # Anonymous Function
map(round, [1.2, 3.5, 1.7]) # Mathematical Map
filter(even, [1, 2, 3, 4]) # Boolean Filter
foreach(println, ['a', 'b']) # Foreach Element
function sumdiff(x, y)
    x + y, x - y # Multiple Return Values
end
x, y = sumdiff(10, 5) # Tuple Destructuring
∘(f, g) = (x...) -> f(g(x...)) # Composition
function sum(x, args...) # Variable Arguments
    sum = x
    for arg in args
        sum += arg
    end
    return sum
end
sum(1, 2, 3, 4); sum(A...)
# Optional Arguments
function parse(T::Type{<:Real}, num::String, base::Int=2)
    # Unsupported: Negative, Decimal, Exponential
    res = T(num[end]) - 48
    for i in 1:(length(num) - 1)
        res += T(num[end - i] - 48) * base^i
    end
    return res
end
parse(Int, "1101"); parse(Int, "1101", 10)
# Keyword Arguments
function parse(T::Type{<:Real}, num::String; base::Int=2)
    # Unsupported: Negative, Decimal, Exponential
    res = T(num[end]) - 48
    for i in 1:(length(num) - 1)
        res += T(num[end - i] - 48) * base^i
    end
    return res
end
parse(Int, "1101"); parse(Int, "1101", base=10)
# Do-Block Syntax
open("outfile.txt", "w") do io
    write(io, "Hello There!")
end

#= Vectorized Functions =#
[1, 2, 3] .^ 3; (^).([1, 2, 3], 3) # Vectorized "Dot" Operation
broadcast(x -> x^3, [1, 2, 3]) # Broadcast Operation (<=> "Dot")
even(x) = x % 2; even.([1, 2, 3]) # Vectorized User-Defined Function
⊗(A, B) = kron(A, B) # Vectorized User-Defined Operator

#= Control Flow =#
# Compound Expressions: begin and (;)
z = begin
    x = 1
    y = 2
    x + y
end
z = (x = 1; y = 2; x + y)
# Conditional Evaluation: if-elseif-else and ?:
if x > 0
    println("$x is positive")
else
    println("$x is negative")
end
println(x, x > 0 ? " is positive" : " is negative")
# Short-Circuit Evaluation: && and ||
x > 0 && println("$x is positive") # <cond> 'and then' <statement>
x > 0 || println("$x is negative") # <cond> 'or else' <statement>
function factorial(n::Int)
    n <= 1 && return 1
    return  n * factorial(n - 1)
end
# Repeated Evaluation (Loops): while and for
i = 1
while i <= 5
    # Body
    i += 1
end
for i = 1:5
    # Body
end
for i in [1, 2, 3] # i ∈ [1, 2, 3]
    # Body
end
for i = 1:2, j = 3:4 # Nested For-Loop
    println((i, j))
end
# Exception Handling: try-catch, error(), throw()
sqrt(-1) # Throws DomainError {x | x <= 0}
struct MyCustomException <: Exception end
f(x) = x >= 0 ? exp(-x) : throw(DomainError())
throw(UndefVarError(:x)) # Requires Argument
struct MyUndefVarError <: Exception
    var::Symbol
end
showerror(io::IO, e::MyUndefVarError) = print(io, e.var, " not defined")
info("Information"); warn("Warning"); error("ErrorException")
f(x) = try
    sqrt(x)
catch
    sqrt(complex(x, 0))
end
f(1); f(-1) # Returns 1.0; 0.0 + 1.0im
f(x) = x >= 0 ? sqrt(x) : sqrt(complex(x, 0))
try error() catch; #= ... =# end
file = open("file_name.txt")
try #= ... =# finally close(file) end
# Tasks (Coroutines)
function producer(c::Channel)
    put!(c, "Start")
    for n = 1:4
        put!(c, 2n)
    end
    put!(c, "Stop")
end
chnl = Channel(producer)
take!(chnl) # Returns "Start"
take!(chnl); take!(chnl); take!(chnl); take!(chnl) # 2, 4, 6, 8
take!(chnl) # Returns "Stop"
for x in Channel(producer) # Channel - Iterable Object
    println(x)
end

#= Scope of Variables =#
local x; global x # Local/Global Scope
let x = 10 # Introduces Local Scope
    while x < 20
        x += 1
    end
end
println(x) # Throws UndefVarError
const e = 2.71828182845904523536 # Optimization

#= Types (Julia Type System) =#
println((1 + 2)::Int) # Type Assertion
x::Float64 = 10 # Type Declaration
function sinc(x)::Float64
    x == 0 && return 1
    return sin(π * x)/(π * x)
end
abstract type MyAbstractType end
abstract type MySubAbstractType <: MyAbstractType end
 # (AbstractFloat, Integer, Irrational, Rational) <: Real <: Number <: Any
 # (BigFloat, Float16, Float32, Float64) <: AbstractFloat
 # (BigInt, Bool, Signed, Unsigned) <: Integer
 # (Int8, Int16, Int32, Int64) <: Signed ('UInt' -> Unsigned)
primitive type MyPrimitiveType <: MyAbstractType 64 end
struct Point # Composite Type
    x::Real
    y::Real
end
pos = Point(1, 2) # Constructor
fieldnames(pos) # Field Names List
println((pos.x, pos.y)) # Access Field Values
mutable struct MutablePoint # Mutable Struct
    x::Real
    y::Real
end
pos = MutablePoint(1, 2); pos.x = 2
IntOrString = Union{Int, AbstractString} # Type Union (Abstract)
struct ParametricPoint{T} # Parametric Composite Type
    x::T
    y::T
end
pos = ParametricPoint{Int}(1, 2)
Array{Float64}; Array{Real} # Invariant Type Parameters
 # Array{Float64}: Contiguous Memory Block
 # Array{Real}: Pointer Array
function norm(pos::Point{<:Real}) # Accept Subtypes
    sqrt(pos.x^2 + pos.y^2)
end
struct Rational{T<:Integer} <: Real
    num::T
    den::T
end
typeof((1, 1.0)); Tuple{Int64, Float64} # Tuple Type
Tuple{Int64, Vararg{Int}} # Tuple{Int64, Vararg{Int64, N} where N}
typeof((1, 2, 3)); NTuple{3, Int64} # Shorthand Notation
NamedTuple{(:a, :b), Int64, Float64} # Named Tuple Type
ages = (andrew=20, ryan=16); ages.ryan == ages[:ryan]
isa(Float64, Type{Float64}) # Singleton Type
ParametricPoint{Int} <: ParametricPoint # UnionAll Type
 # ParametricPoint <-> ParametricPoint{T} where T
function norm(pos::Point{T}) where T<:Real
    sqrt(pos.x^2 + pos.y^2)
end
 # Type Variable Bounds: Array{T} where Int<:T<:Number
 # Shorthand Notation: Array{<:Integer} <-> Array{T} where T<:Integer
 # Vector: const Vector = Array{T,1} where T | Vector{T} = Array{T,1}
const Int = Int64 # Type Alias (64-Bit)
isa(1.0, Float64); typeof(DataType); supertype(Float64); subtypes(Real)
struct Polar{T<:Real} <: Number # Complex Number Polar Form
    r::T
    Θ::T
end
Polar(r::Real, Θ::Real) = Polar(promote(r, Θ)...)
Base.show(io::IO, z::Polar) = print(io, z.r, " * exp(", z.Θ, "im)")

#= Methods (Multiple Dispatch) =#
# Multiple Dispatch: Inspect All Function Arguments -> Invoke Method
 # Java: method(this, args...) has implicit first argument: this
 # Python: method(self, args...) has explicit first argument: self
 # => Single Dispatch: obj.method(args...) -> method(obj, args...)
f(x::Float64, y::Float64) = 2x + y
f(1, 2) # Throws MethodError
f(x::Number, y::Number) = 2x + y
f(1, 2.0) # Method Selection
methods(f) # Method Signatures
g(x::Float64, y) = 2x + y
g(x, y::Float64) = 2x + y
g(1.0, 2.0) # Method Ambiguity
same_type(x::T, y::T) where {T} = true # Parametric Method
same_type(x, y) = false
append(v::Vector{T}, x::T) where {T} = [v..., x]
typeof(x::T) where {T} = T
 # For 1 Argument: where {T} == where T (Optional {})
 # For 2 Arguments: where {T, S} == where S where T (Nested)
getindex(A::AbstractArray{T, N}, indexes::Vararg{Number, N}) where {T, N} = A[indexes...]
struct Polynomial{R}
    coeffs::Vector{R}
end
function (poly::Polynomial)(x) # Functor (Ex: Constructor)
    polyval = poly.coeffs[1]
    for i = 2:length(poly.coeffs)
        polyval += poly.coeffs[i] * x^(i - 1)
    end
    return polyval
end
poly = Polynomial([1, 10, 100]); poly(3)
Point(x) = Point(x, 0) # Outer Constructor
struct OrderedPair # Inner Constructor
    x::Real
    y::Real
    OrderedPair(x, y) = x > y ? error("Constraint: x <= y") : new(x, y)
end
mutable struct SelfReferential
    obj::SelfReferential
    SelfReferential() = (x = new(); x.obj = x)
end
struct ConstructPoint{T<:Real} # Parametric Constructor
    x::T
    y::T
    ConstructPoint{T}(x, y) where {T<:Real} = new(x, y)
end
Point(x::T, y::T) where {T<:Real} = Point{T}(x, y)
Point(x::Real, y::Real) = Point(promote(x, y)...)

#= Conversion and Promotion =#
convert(Int64, 12.0); Int64(12.0)
Base.convert(::Type{Bool}, x::Real) = x == 0 ? false : x == 1 ? true : throw(InexactError())
promote(1, 2.5); promote(2.0, 3//4); promote(1 + 2im, 1.5)
+(x::Number, y::Number) = +(promote(x, y)...)
Base.promote_rule(::Type{Float64}, ::Type{Float32}) = Float64

#= Multi-Dimensional Arrays =#
A = Array{Int}(undef, 2) # Uninitialized Two-Dimensional Array
zeros(Int, 2); ones(Int, 2) # Array Construction
B = Matrix{I, 2, 2} # 'Explicit' Identity Matrix
fill("Value", 2); fill!(A, "Value") # Fill Array
rand(Int, 2); randn(Int, 2) # Uniform, Normal Distributions
eltype(A); length(A); ndims(A); size(A) # Array Functions
reshape(A, 3) # Reshape Array (Modify Dimensions)
linspace(1, 10, 10) # Linearly Spaced Elements (start, stop, step)
B = copy(A); cat(1, A, B) # Concatenate Along Specified Dimension
vcat(A, B); cat(1, A, B); [A; B] # Vertical Concatenation
hcat(A, B); cat(2, A, B); [A B] # Horizontal Concatenation
transpose(A); adjoint(A); A' # [Conjugate] Transpose
Int[1, 2, 3, 4] # Initialize Typed Array
squares = [i^2 for i = 1:10] # Comprehension
squares = (i^2 for i = 1:10) # Generator (No Memory Allocation)
println(sum(squares)) # Memory Allocated On-Demand
even_squares = (i^2 for i = 1:10 if i % 2 == 0) # Filtering
getindex(A, 1); A[1] # Array Indexing
setindex!(A, 10, 1); A[1] = 10 # Array Assignment
C = [1, 2; 3, 4]; println(C[2, :]) # Array Splicing
for (index, value) in enumerate(A) # Enumeration
    println("A[$index] = $value")
end
push!(); pop!(); insert!(); splice!() # List Operations
union(); intersect(); setdiff(); issubset() # Set Operations {∪, ∩, ⊆}
issubset(); in() # Subset Operations {⊆, ⊈, ⊇, ⊉}, Set Member {∈}

#= Interfaces (Iteration) =#
function Base.iterate(iter, state=(iter.start, 0))
    element, index = state
    index >= iter.length && return nothing
    return (element, (f(element), index + 1))
end
function Base.iterate(iter, (el, i)=(iter.start, 0))
    return i >= iter.length ? nothing : (el, (f(el), i + 1))
end
struct Squares
    length::Int
end
function Base.iterate(iter::Squares, state=1)
	return state > iter.length ? nothing : (state^2, state + 1)
end
Base.eltype(iter::Squares) = Int
Base.length(iter::Squares) = iter.length
foreach(println, Squares(7))
println(25 in Squares(10))
let iter = Squares(7), next = iterate(iter)
    while next !== nothing
        el, state = next
        println(el) # Body
        next = iterate(iter, state)
    end
end
collect(Squares(10)) # Collection -> Array
Base.sum(S::Squares) = (n = S.count; return n * (n + 1) * (2n + 1) / 6)
println(sum(Squares(1803)))
function Base.getindex(S::Squares, i::Int)
    1 <= i <= S.count || throw(BoundsError(S, i))
    return i^2
end
println(Squares(100)[23])
Base.firstindex(S::Squares) = 1
Base.lastindex(S::Squares) = length(S)
println(Squares(23)[end])

#= Modules =#
 # Julia Load Path: push!(LOAD_PATH, "/Path/To/My/Module/")
 # Julia Startup File: ~/.julia/config/startup.jl
module Coordinates2D
import Base.convert
export Cartesian, Polar
struct Cartesian{T<:Real}
    x::T
    y::T
end
struct Polar{T<:Real}
    r::T # Radius
    θ::T # Angle {Degrees}
end
convert(::Type{Cartesian}, P::Polar) = Cartesian(P.r * cosd(P.θ), P.r * sind(P.θ))
convert(::Type{Polar}, P::Cartesian) = Polar(sqrt(P.x^2 + P.y^2), atand(P.y / P.x))
end

#= Metaprogramming (Expressions) =#
 # Julia Code -> Internally Represented As Data Structure
expr = parse("1 + 1") # Expression
expr.head; exp.args # Expression Fields
dump(expr) # Display Annotated Fields
:(1 + 1) # Quoting -> Create Expr Object
a = 1; :($a + b) # :(1 + b) Interpolation
eval(expr) # Evaluate Expression (Global Scope)
math_expr(op, op1, op2) = :(($op)($op1, $op2))
eval(math_expr(:+, 1, math_expr(:*, 4, 5))) # (1 + 4 * 5)
 # Function: Argument Tuple -> Return Value
 # Macro: Argument Tuple -> Expression
macro greeting()
    return :(println("Hello, Ken!"))
end
@greeting()
macro greeting(name)
    return :(println("Hello, ", $name, "!"))
end
@greeting("Ken")
expr = macroexpand(@greeting) # View Quoted Expression
@macroexpand @greeting # Function Alternative
macro whymacro()
    println("Executed When Parsed")
    return :(println("Executed During Runtime"))
end
expr = macroexpand(:(@whymacro))
eval(expr) # Runtime Execution
macro disp_args(arg1, arg2)
    show(arg1); show(arg2) # Show Argument Expression
end
@disp_args("Argument 1", "Argument 2")
@disp_args "Argument 1" "Argument 2"
macro assert(expr)
    return :($expr ? nothing : throw(AssertionError($(string(expr)))))
end
@assert 1 == 1.0 # 1 == 1.0 ? nothing : throw(AssertionError("1 == 1.0"))
macro assert(ex, msgs...)
    msg_body = isempty(msgs) ? ex : msgs[1]
    msg = string(msg_body)
    return :($ex ? nothing : throw(AssertionError($msg)))
end
@assert (a = 1; b = 1.0; a == b) "a should equal b!"
macro time(expr) # Macro Hygiene (Variable Scope)
    return quote
        local t0 = time()
        local val = $expr
        local tn = time()
        println("Elapsed Time: ", tn - t0, "s")
        val
    end
end
macro zerox() # Violate Hygiene
    return esc(:(x = 0))
end
x = 10; @zerox; println(x) # Modified Variable
eval(:(1 + 1)); @eval (1 + 1) # eval(quote(...))